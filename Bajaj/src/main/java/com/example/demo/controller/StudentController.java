package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.request.StudentRequest;
import com.example.demo.response.StudentResponse;

@RestController
public class StudentController {

	@PostMapping("/bfhl")
	public StudentResponse getStudentDetail(@RequestBody StudentRequest data) {
		List<String> alphabetics = new ArrayList<String>();
		List<String> numbers = new ArrayList<String>();
		for (String str : data.getData()) {
			if (isNum(str)) {
				numbers.add(str);
			} else {
				alphabetics.add(str);
			}
		}
		StudentResponse response = new StudentResponse();
		response.setIs_success(true);
		response.setUser_id("Ram_24071992");
		response.setEmail("ram.landge2407@gmail.com");
		response.setRoll_number("Ram123");
		response.setNumbers(numbers);
		response.setAlphabets(alphabetics);
		return response;
	}

	public static boolean isNum(String strNum) {
		boolean ret = true;
		try {
			Double.parseDouble(strNum);

		} catch (NumberFormatException e) {
			ret = false;
		}
		return ret;
	}
}
