package com.example.demo.request;

import java.util.List;

public class StudentRequest {

	private List<String> data;

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "StudentRequest [data=" + data + "]";
	}
	
	
	
}
